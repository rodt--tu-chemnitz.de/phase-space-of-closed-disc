# Phase Space of Closed Disc

Husimi and Wigner functions are used to describe the phase space of a closed disc. 

## Hot to use

Execute the code with
```
python main.py m n
```
to obtain the field and quasi phase-spaces for a mode of $`m`$-th order and $`n`$-th lowest value for $`k`$.

To calculate the values use `vary_params.sh`, where a wide range of $`m`$ and $`n`$ are examined and saved appropriately

## Theory

The modes of a closed disc can be written as
```math
E_z^{m,k}(r, \phi) = J_v(m, k r) \mathrm{e}^{\mathrm{i} m \phi}
```
where $`k`$ must be chosen so that $`J_v(m, k R) = 0`$, with the radius of the disc $`R`$. The mode is represented using the boundary function
```math
u(s) = \vec{n}(s) \cdot \mathrm{grad} \, E_z^{m,k}(R, \phi(s))
```
with arc length $`s \in [ 0, 1)`$ and normal vector $`\vec{n}(s)`$. This is then used to calculate the pseudo phase-space functions.

### Husimi:

The Husimi function is described by
```math
h(q, p) = \frac{1}{2 \pi k} \, | \int_{0}^{1} c_{(q,p),k}(s) \, u(s) \, \mathrm{d}s |^2
```
with coherent state $`c_{(q,p),k}(s) = (\frac{k}{\pi})^\frac{1}{4} \sum_{l \in \mathcal{Z}}  \mathrm{e}^{\mathrm{i} k p (s-q+l)} \mathrm{e}^{- \alpha k (s-q+l)^2}`$, in which the sum reflects the periodic boundary conditions of the disc surface. $`\alpha`$ can be used to specify the width of the gaussian-shaped coherent state. Here, $`\alpha = 0.05`$ is chosen.

This represents a fourier transformation of $`u(s) \, \mathrm{e}^{- \alpha k (s-q+l)^2}`$, as the boundary conditions are periodic and the phase factor $`\mathrm{e}^{\mathrm{i} k p s-q+l}`$ can be neglected. This is taken advantage of by using the `numpy.fft` module , which leads to a very fast integration of the equation above. Note that the resulting amount of the data points for the Husimi function depends on sampling rate and number of samples.


### Wigner:

The Wigner function is determined by calculating
```math
w(q, p) = \frac{1}{2 \pi \hbar} \int_{-\infty}^{\infty} \mathrm{e}^{\mathrm{i} k p \xi} \, u(q - \xi) \, u(q +\xi) \, \mathrm{d}\xi
```
where it must be kept in mind that $`u(q) = u(q+1)`$. This way the equation can be calculated using FFT, applying the module mentioned above.

## Example

The following graphics are calculated with the parameters $`m=3`$, $`k = 6.38`$.

### Eigenmode

![](readme_pictures/mvar_eigenmode_m=3_ki=0.png?raw=true)

### Boundary Function

![](readme_pictures/mvar_boundaryfunc_m=3_ki=0.png?raw=true)

### Husimi Function

![](readme_pictures/mvar_husimi_m=3_ki=0.png?raw=true)

### Wigner Function

![](readme_pictures/mvar_wigner_m=3_ki=0.png?raw=true)