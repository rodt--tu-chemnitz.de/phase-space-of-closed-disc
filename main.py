import modules.plot_eigenmode as plot_eigenmode
import modules.plot_boundaryfunc as plot_boundaryfunc
import modules.plot_phasespace_husimi as plot_phasespace_husimi
import modules.plot_phasespace_wigner as plot_phasespace_wigner

import scipy
import sys

def main():
    print('---- MAIN FUNCTION ----')
    print()

    m = int(sys.argv[1])
    k_index = int(sys.argv[2])

    k_available = scipy.special.jn_zeros(m, k_index+1)
    k = k_available[k_index]

    args = dict(m=m, k=k)

    print('=> m = {0}'.format(m))
    print('=> k = {0}'.format(k))
    print()

    plot_eigenmode.main(**args)
    print()

    plot_boundaryfunc.main(**args)
    print()
    
    plot_phasespace_husimi.main(
        **args,
        plot_coherent_state=False,
        plot_integral=True,
        plot_slice=True,
        plot_phasespace=True
        )
    print()

    plot_phasespace_wigner.main(
        **args,
        plot_integral=True,
        plot_slice=True,
        plot_phasespace=True
        )
    print()



if __name__ == '__main__':
    main()