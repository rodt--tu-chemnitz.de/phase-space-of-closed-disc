import modules.plotparams

import modules.toolbox as tb

import numpy as np
import matplotlib.pyplot as plt

import scipy



def main(m, k):
    print('PLOTTING BOUNDARY FUNCTION')

    # data
    print('\tcalculating data...')

    n_points = 201
    s_list = np.linspace(0, 1, n_points)
    phi_list = np.linspace(0, 2* np.pi, n_points)

    b_list = [tb.boundary_func(s, k, m) for s in s_list]

    surface_list = tb.surface_point(s_list)
    grad_list = [tb.eigenmode_cart_grad(*p, k, m) for p in surface_list]
    norm_list = [np.linalg.norm(v) for v in grad_list]
    

    print('\tplotting...')

    fig, ax = plt.subplots(
        # subplot_kw={'projection': 'polar'},
        figsize=(4,3)
    )

    ax.plot(
        s_list,
        b_list,
        label='$u(s)$'
        )

    # ax.legend(loc='upper right')

    ax.set_xlabel('$s$')
    ax.set_xlim(0,1)

    fig.savefig('boundaryfunc.png')
    plt.close()

    print('\tDone!')



if __name__ == '__main__':
    m = 8
    k_available = scipy.special.jn_zeros(m, 10)
    k = k_available[0]

    main(m=m, k=k)