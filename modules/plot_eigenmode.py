import modules.plotparams

import modules.toolbox as tb

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

import scipy

def main(m, k):
    print('PLOTTING EIGENMODE')

    print('\tcalculating data...')

    # data

    x_coords = np.linspace(-1.1, 1.1, 201)
    y_coords = np.linspace(-1.1, 1.1, 201)

    args = dict(k=k,m=m)

    z = tb.grid(tb.eigenmode_cart_trunc, x_coords, y_coords, args)

    # print(z)

    z_min = np.abs( np.min(z) )
    z_max = np.abs( np.max(z) )
    limit = np.max([z_min, z_max])
    levels = np.linspace(-limit, limit, 8)



    ###############################
    # plotting
    ###############################

    print('\tplotting...')

    fig, ax = plt.subplots(
        )

    ax.contourf(
        x_coords, y_coords, z,
        cmap=plt.get_cmap('coolwarm'),
        levels=levels,
        vmin=-limit,
        vmax=+limit
        )

    # plotting circle

    p = mpatches.Circle(
        xy=[0,0],
        radius=tb.R,
        fill=False,
        edgecolor='black',
        linewidth=4.
        )
    ax.add_patch(p)

    ax.set_aspect('equal')

    fig.savefig('eigenmode.png')
    plt.close()

    print('\tDone!')


if __name__ == '__main__':
    m = 8
    k_available = scipy.special.jn_zeros(m, 10)
    k = k_available[0]

    main(m=m, k=k)