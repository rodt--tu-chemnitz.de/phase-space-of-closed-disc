import modules.plotparams

import modules.toolbox as tb

import numpy as np
import matplotlib.pyplot as plt

import scipy




def main(m, k,
        plot_coherent_state=True,
        plot_integral=True,
        plot_slice=True,
        plot_phasespace=True
        ):
    print('PLOTTING PHASESPACE')


    ################################################
    # coherent state
    ################################################

    if plot_coherent_state:
        print('\tsingle coherent state')

        print('\t\calculating data...')
        s = .5

        q_list = np.linspace(0, 1, 201)
        p_list = np.linspace(-1, 1, 201)

        z_list = []
        for p in p_list:
            z_sublist = [tb.coherent_state(s, q, p, k,type='periodic') for q in q_list]

            z_list.append(z_sublist)

        print('\t\tplotting...')

        fig, ax = plt.subplots()

        ax.contourf(
            q_list,
            p_list,
            z_list,
            cmap=plt.get_cmap('coolwarm'),
            )

        fig.savefig('coherent_state.png')
        plt.close()
    
    ################################################
    # integral
    ################################################

    q  = 0.5
    rate = tb.husimi_rate(k)
    xi_min = q - 0.5
    xi_max = q + 0.5
    xi_list = np.arange(xi_min, xi_max+rate, rate)

    if plot_integral:
        y_list = [tb.husimi_integrand(q, k, m, xi) for xi in xi_list]
        
        y_list_boundary = [tb.boundary_func(xi, k, m) for xi in xi_list]
        y_list_coherent = [tb.husimi_gaussian(xi, q, k) for xi in xi_list]


        fig, ax = plt.subplots()


        ax.plot(xi_list, y_list_boundary, label='boundary func', linestyle='--')
        ax.plot(xi_list, y_list_coherent, label='coherent state', linestyle='--')

        ax.plot(
            xi_list, 
            y_list, 
            label='product'
            )

        ax.text(
            0.05, 0.95, '$m={0}$, $k={1:.3f}$\n$q={2:.2f}$'.format(m, k, q),
            bbox=dict(
                boxstyle="round",
                ec='lightgray',
                fc='white',
                ),
            transform=ax.transAxes,
            ha='left',
            va='top'
            )
        
        ax.set_xlim(np.min(xi_list), np.max(xi_list))

        ax.set_xlabel(r'$\xi$ [L]')
        ax.set_ylabel(r'')

        fig.savefig('husimi_integral.png')
        plt.close()
    
    ################################################
    # slice
    ################################################

    if plot_slice:
        print('\thusimi function slice')

        print('\t\tcalculating data...')
        # xi_list = np.linspace(0, xi_max, tb.wigner_n_integralpoints)
        y_list = [tb.husimi_integrand(q, k, m, xi) for xi in xi_list]

        fft_y = np.fft.fftshift(np.fft.fft(y_list) * rate)

        n_frequencies = fft_y.size
        f_list = np.fft.fftshift(np.fft.fftfreq(n_frequencies, d=rate)) / k


        print('\t\tplotting...')

        fig, ax = plt.subplots()

        ax.plot(f_list,np.real(fft_y),label='real',marker='o')
        
        ax.plot(f_list,np.imag(fft_y),label='imag',marker='o')
        
        ax.plot(f_list,np.abs(fft_y),label='norm',marker='o', color='black')
        
        for x in np.array([-1,1]) * m/k:
            ax.axvline(
                x,
                linestyle='--',
                color='tab:red',
                zorder=-2
                )
        
        ax.legend()

        ax.set_xlim(-1, 1)

        # weird order of ticks so 0 gets precedent over m/k in case of m = 0
        ax.set_xticks([-1, -m/k, m/k, 0, 1])
        ax.set_xticklabels(
            [-1, '$-m/k$', '$m/k$', 0, 1]
            )
        
        ax.set_xlabel(r'$p$ [$k_n$]')

        ax.text(
            0.05, 0.95, '$m={0}$, $k={1:.3f}$, $q={2:.2f}$'.format(m, k, q),
            bbox=dict(
                boxstyle="round",
                ec='lightgray',
                fc='white',
                ),
            transform=ax.transAxes,
            ha='left',
            va='top'
            )

        fig.savefig('husimi_slice.png')
        plt.close()

    ################################################
    # phase space
    ################################################

    if plot_phasespace:
        print('\thusimi function')

        print('\t\tcalculating data...')

        rate_now = tb.husimi_rate(k)
        q_list = np.linspace(0,1,51)
        fft_list = []

        for q_now in q_list:
            xi_min = q_now - 0.5
            xi_max = q_now + 0.5
            xi_list_now = np.arange(xi_min, xi_max+rate_now, rate_now)
            y_list = [tb.husimi_integrand(q_now, k, m, xi) for xi in xi_list_now]

            fft_y = np.fft.fftshift(np.fft.fft(y_list) * rate_now)
            fft_list.append(np.abs(fft_y))

        # print([len(x) for x in fft_list])
        fft_list = np.array(fft_list)

        n_frequencies = fft_y.size
        f_list = np.fft.fftshift(np.fft.fftfreq(n_frequencies, d=rate_now)) / k

        print('\t\tplotting...')        

        fig, ax = plt.subplots()

        ax.contourf(
            q_list,
            f_list,
            fft_list.T,
            cmap=plt.get_cmap('inferno')
            )

        ax.set_xlabel(r'$q$')
        ax.set_ylabel(r'$p$')

        ax.set_xticks([0, .5, 1])

        ax.set_ylim(-1, 1)

        
        # weird order of ticks so 0 gets precedent over m/k in case of m = 0
        ax.set_yticks([-1, -m/k, m/k, 0, 1])
        ax.set_yticklabels(
            [-1, '$-m/k$', '$m/k$', 0, 1]
            )

        ax.text(
            0.05, 0.95, '$m={0}$, $k={1:.3f}$'.format(m, k),
            bbox=dict(
                boxstyle="round",
                ec='black',
                fc='white',
                ),
            transform=ax.transAxes,
            ha='left',
            va='top'
            )

        fig.savefig('husimi.png')
        plt.close(fig)
    

    print('\tDone!')



if __name__ == '__main__':
    m = 20
    k_available = scipy.special.jn_zeros(m, 10)
    k = k_available[0]

    main(m=m, k=k)