import modules.plotparams

import modules.toolbox as tb

import numpy as np
import matplotlib.pyplot as plt

import scipy




def main(m, k,
        plot_integral=True,
        plot_slice=True,
        plot_phasespace=True,
        ):
    
    print('PLOTTING WIGNER PHASESPACE')

 
    ################################################
    # integral
    ################################################

    q  = 0.25

    if plot_integral:
        print('\twigner integral at (s,p)')

        # xi_list = np.linspace(0, xi_max, tb.wigner_n_integralpoints)
        xi_list, rate = tb.wigner_xi_list(k)
        xi_max = xi_list[-1]

        y_list = []

        for xi in xi_list:
            val = tb.wigner_integrand(q, k, m, xi, xi_max)
            y_list.append(val)

        print('\t\tplotting...')

        fig, ax = plt.subplots()

        ax.plot(
            xi_list,
            y_list,
            label='real'
            )
        
        ax.set_xlim(xi_list[0], xi_list[-1])

        ax.set_xlabel(r'$\xi$ [$L$]')
        ax.set_ylabel(r'$u(q-\xi/2) \, u(q+\xi/2) \, g(\xi)$')

        ax.text(
            0.05, 0.95, '$m={0}$, $k={1:.3f}$\n$q={2:.2f}$'.format(m, k, q),
            bbox=dict(
                boxstyle="round",
                ec='lightgray',
                fc='white',
                ),
            transform=ax.transAxes,
            ha='left',
            va='top'
            )

        fig.savefig('wigner_integral.png')
        plt.close()
    

    ################################################
    # slice
    ################################################
    if plot_slice:
        print('\twigner function slice')

        print('\t\tcalculating data...')
        # xi_list = np.linspace(0, xi_max, tb.wigner_n_integralpoints)
        xi_list, rate = tb.wigner_xi_list(k)

        y_list = [tb.wigner_integrand(q, k, m, xi, xi_max) for xi in xi_list]

        fft_y = np.fft.fftshift(np.fft.fft(y_list) * rate)

        n_frequencies = fft_y.size
        f_list = np.fft.fftshift(np.fft.fftfreq(n_frequencies, d=rate)) / k


        print('\t\tplotting...')

        fig, ax = plt.subplots()

        ax.plot(f_list,np.real(fft_y),label='real',marker='o')
        
        ax.plot(f_list,np.imag(fft_y),label='imag',marker='o')
        
        ax.plot(f_list,np.abs(fft_y),label='norm',marker='o', color='black')
        
        for x in np.array([-1,1]) * m/k:
            ax.axvline(
                x,
                linestyle='--',
                color='tab:red',
                zorder=-2
                )
        
        ax.legend()

        ax.set_xlim(-1, 1)

        # weird order of ticks so 0 gets precedent over m/k in case of m = 0
        ax.set_xticks([-1, -m/k, m/k, 0, 1])
        ax.set_xticklabels(
            [-1, '$-m/k$', '$m/k$', 0, 1]
            )
        
        ax.set_xlabel(r'$p$ [$k_n$]')

        ax.text(
            0.05, 0.95, '$m={0}$, $k={1:.3f}$, $q={2:.2f}$'.format(m, k, q),
            bbox=dict(
                boxstyle="round",
                ec='lightgray',
                fc='white',
                ),
            transform=ax.transAxes,
            ha='left',
            va='top'
            )

        fig.savefig('wigner_slice.png')
        plt.close()




    ################################################
    # phase space
    ################################################

    if plot_phasespace:
        print('\twigner function')

        print('\t\tcalculating data...')

        xi_list, rate = tb.wigner_xi_list(k)
        xi_max = xi_list[-1]

        q_list = np.linspace(0,1,51)
        fft_list = []

        for q_now in q_list:
            # xi_list = np.linspace(0, xi_max, tb.wigner_n_integralpoints)
            y_list = [tb.wigner_integrand(q_now, k, m, xi, xi_max) for xi in xi_list]

            fft_y = np.fft.fftshift(np.fft.fft(y_list) * rate)
            fft_list.append( np.power(np.abs(fft_y),2) )

        # print([len(x) for x in fft_list])
        fft_list = np.array(fft_list)

        n_frequencies = fft_y.size
        f_list = np.fft.fftshift(np.fft.fftfreq(n_frequencies, d=rate)) / k

        print('\t\tplotting...')        

        fig, ax = plt.subplots()

        ax.contourf(
            q_list,
            f_list,
            fft_list.T,
            cmap=plt.get_cmap('inferno')
            )

        ax.set_xlabel(r'$q$')
        ax.set_ylabel(r'$p$')

        ax.set_xticks([0, .5, 1])

        ax.set_ylim(-1, 1)

        
        # weird order of ticks so 0 gets precedent over m/k in case of m = 0
        ax.set_yticks([-1, -m/k, m/k, 0, 1])
        ax.set_yticklabels(
            [-1, '$-m/k$', '$m/k$', 0, 1]
            )

        ax.text(
            0.05, 0.95, '$m={0}$, $k={1:.3f}$'.format(m, k),
            bbox=dict(
                boxstyle="round",
                ec='black',
                fc='white',
                ),
            transform=ax.transAxes,
            ha='left',
            va='top'
            )

        fig.savefig('wigner.png')
        plt.close(fig)

    print('\tDone!')



if __name__ == '__main__':
    m = 1
    k_available = scipy.special.jn_zeros(m, 10)
    k = k_available[0]

    main(m=m, k=k)