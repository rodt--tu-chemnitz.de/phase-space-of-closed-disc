import matplotlib

matplotlib.rcParams['savefig.dpi'] = 300
matplotlib.rcParams['savefig.bbox'] = 'tight'

matplotlib.rcParams['xtick.direction'] = 'inout'
matplotlib.rcParams['xtick.top'] = True
matplotlib.rcParams['ytick.direction'] = 'inout'
matplotlib.rcParams['ytick.right'] = True

matplotlib.rcParams['axes.axisbelow'] = False

matplotlib.rcParams['grid.color'] = 'black'
matplotlib.rcParams['grid.linestyle'] = 'dashed'