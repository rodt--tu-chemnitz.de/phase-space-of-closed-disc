import numpy as np
from scipy.special import jv


###################################################
# mode functions
###################################################

def eigenmode_cyl(r, phi, k, m):
    '''
    Eigenmode of closed disk in cylinder coords.
    '''
    return np.real(jv(m, r*k) * np.exp(1j * phi * m))



ex = np.array([1,0])
def eigenmode_cart(x, y, k, m):
    '''
    Eigenmode of closed disk in .
    '''
    r_vec = np.array([x,y])
    r = np.linalg.norm(r_vec)

    if r < 10**(-6):
        phi = 0.
    else:
        phi = np.arccos( np.dot(r_vec, ex) / r)
        if x < 0.:
            phi *= -1.
    
    return eigenmode_cyl(r, phi, k, m)


R = 1.
def eigenmode_cart_trunc(x, y, k, m):
    '''
    Eigenmode of closed disk with R=1. in cartesian coords.
    '''
    r = np.linalg.norm([x,y])

    if r < R:
        return eigenmode_cart(x, y, k, m)
    else:
        return 0.



# best value for step size to determine derivative
h_best = 100. * np.finfo(float).eps
# print('h_best = {0}'.format(h_best))
def eigenmode_cart_grad(x,y,k,m):
    '''
    Eigenmode of closed disk in cartesian coords.
    '''

    args = dict(k=k, m=m)

    # x derivative

    x_1 = x + h_best
    x_2 = x - h_best
    x_delta = x_1 - x_2

    f_1 = eigenmode_cart(x_1, y, **args)
    f_2 = eigenmode_cart(x_2, y, **args)
    x_der = (f_1 - f_2) / x_delta

    # y derivative
    
    y_1 = y + h_best
    y_2 = y - h_best
    y_delta = y_1 - y_2

    f_1 = eigenmode_cart(x, y_1, **args)
    f_2 = eigenmode_cart(x, y_2, **args)
    y_der = (f_1 - f_2) / y_delta

    return np.array([x_der,y_der])



###################################################
# othe useful stuff
###################################################

def grid(f, x_list, y_list, args):
    '''
    function applied to a grid
    '''
    grid = []

    for x in x_list:
        grid_sub = []
        for y in y_list:
            z = f(
                x=x,
                y=y,
                **args
                )
            grid_sub.append(z)
        grid.append(grid_sub)
    
    return grid



def surface_point(s):
    '''
    Returns surface point of a sphere with radius R
    with s in [0, 1]
    '''
    angle = s * 2. * np.pi
    x = R * np.cos(angle)
    y = R * np.sin(angle)

    outlist = np.array([x,y]).T

    return outlist



def surface_normal_vector(s):
    '''
    Returns surface normal vector of a sphere with
    center at (0,0) and with s in [0, 1]
    '''
    angle = s * 2. * np.pi
    x = np.cos(angle)
    y = np.sin(angle)

    outlist = np.array([x,y]).T

    return outlist



def boundary_func(s, k, m):
    '''
    Poincare section at the boundary. Describes the eigenfunction inside
    the cavity. Used to calculate the Husimi function.
    '''
    v_normal = surface_normal_vector(s)

    p_surface = surface_point(s)
    v_gradient = eigenmode_cart_grad(*p_surface, k, m)

    out = v_normal @ v_gradient

    return out



###################################################
# husimi function stuff
###################################################

gaussian_widthfac = .05
def husimi_gaussian(xi, q, k):
    c = np.power( k / np.pi, 0.25 )
    
    sum_list = []
    for i in np.arange(-10, 11, 1):
        delta = (xi - q + i) * 2. * np.pi
        exparg = - k * delta**2 * gaussian_widthfac
        expval = np.exp(exparg)
        sum_list.append(expval)
    
    return c * np.sum(sum_list)

def husimi_integrand(q, k, m, xi):
    '''
    function to be fourier transformed in order to obtain husimi function

    '''
    
    out = husimi_gaussian(xi,q,k) * boundary_func(xi, k, m) # * np.exp(1j * k * q)

    return out


def husimi_rate(k):
    xi_list, rate = wigner_xi_list(k)
    return rate


###################################################
# wigner function stuff
###################################################


def wigner_integrand(q,k,m,xi,xi_max):
    '''
    function to be fourier transformed for wigner function
    '''

    # main function
    boundfunc = boundary_func(q-0.5*xi, k, m) * boundary_func(q+0.5*xi, k, m)

    # gaussian factor, so borders dont matter at fourier transform
    gauss_mu = xi_max * .5
    gauss_sig = gauss_mu * .3 # * 2 * .5 -> 
    gauss_var = np.power(gauss_sig, 2)
    gauss_fac = gaussian(xi, gauss_mu, gauss_var)

    # calculate out
    val = boundfunc * gauss_fac
    return val



def gaussian(x, mu, var):
    '''
    gaussian for modulation
    '''
    return np.exp(-np.power(x - mu, 2.) / (2 * var)) / np.sqrt( 2. * np.pi * var)


wigner_n_ftpoints = 200
def wigner_xi_list(k):
    n_datapoints = wigner_n_ftpoints
    xi_max = 20. * np.pi / k

    xi_list = np.linspace(0, xi_max, n_datapoints)
    rate = np.mean(xi_list[1:] - xi_list[:-1])
    # print('rate = {0:.3f}'.format(rate))
    
    return [xi_list, rate]