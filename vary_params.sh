outdir="vary_params"

outdir_general=$outdir/general
outdir_husimi=$outdir/func_husimi
outdir_wigner=$outdir/func_wigner

mkdir -p $outdir_general
mkdir -p $outdir_husimi
mkdir -p $outdir_wigner

###################################################
# VARY M
###################################################

# wave number index
k_index=0

for m in $(seq 0 20); do
    # execute code
    params="$m $k_index"
    python main.py $params

    # move output
    mv eigenmode.png $outdir_general/mvar_eigenmode_m=${m}_ki=${k_index}.png
    mv boundaryfunc.png $outdir_general/mvar_boundaryfunc_m=${m}_ki=${k_index}.png

    mv husimi.png $outdir_husimi/mvar_husimi_m=${m}_ki=${k_index}.png
    mv husimi_integral.png $outdir_husimi/mvar_husimi_integral_m=${m}_ki=${k_index}.png
    mv husimi_slice.png $outdir_husimi/mvar_husimi_slice_m=${m}_ki=${k_index}.png

    mv wigner.png $outdir_wigner/mvar_wigner_m=${m}_ki=${k_index}.png
    mv wigner_integral.png $outdir_wigner/mvar_wigner_integral_m=${m}_ki=${k_index}.png
    mv wigner_slice.png $outdir_wigner/mvar_wigner_slice_m=${m}_ki=${k_index}.png

    echo
    echo
done

###################################################
# VARY K
###################################################

# mode order
m=8

for k_index in $(seq 0 20); do
    # execute code
    params="$m $k_index"
    python main.py $params

    # move output
    mv eigenmode.png $outdir_general/kvar_eigenmode_m=${m}_ki=${k_index}.png
    mv boundaryfunc.png $outdir_general/kvar_boundaryfunc_m=${m}_ki=${k_index}.png

    mv husimi.png $outdir_husimi/kvar_husimi_m=${m}_ki=${k_index}.png
    mv husimi_integral.png $outdir_husimi/kvar_husimi_integral_m=${m}_ki=${k_index}.png
    mv husimi_slice.png $outdir_husimi/kvar_husimi_slice_m=${m}_ki=${k_index}.png

    mv wigner.png $outdir_wigner/kvar_wigner_m=${m}_ki=${k_index}.png
    mv wigner_integral.png $outdir_wigner/kvar_wigner_integral_m=${m}_ki=${k_index}.png
    mv wigner_slice.png $outdir_wigner/kvar_wigner_slice_m=${m}_ki=${k_index}.png

    echo
    echo
done



echo "ALL DONE!"